package ru.oslepov.rss2;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

public class RSSItem implements Parcelable {
    private long id;
    private String title;
    private String description;
    private String imageUrl;
    private String createdAt;

    public static final class RSSItemEntry implements BaseColumns {
        public static final String TABLE_NAME = "rssItems";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_IMAGE_URL = "imageUrl";
        public static final String COLUMN_CREATED_AT = "createdAt";
    }

    public RSSItem(long id, String title, String description, String imageUrl, String createdAt) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    protected RSSItem(Parcel in) {
        id = in.readLong();
        title = in.readString();
        description = in.readString();
        imageUrl = in.readString();
        createdAt = in.readString();
    }

    public static final Creator<RSSItem> CREATOR = new Creator<RSSItem>() {
        @Override
        public RSSItem createFromParcel(Parcel in) {
            return new RSSItem(in);
        }

        @Override
        public RSSItem[] newArray(int size) {
            return new RSSItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(imageUrl);
        dest.writeString(createdAt);
    }
}
