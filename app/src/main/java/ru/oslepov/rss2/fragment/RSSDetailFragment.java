package ru.oslepov.rss2.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.squareup.picasso.Picasso;
import ru.oslepov.rss2.R;
import ru.oslepov.rss2.RSSItem;

/**
 * A simple {@link Fragment} subclass.
 */
public class RSSDetailFragment extends Fragment {
    private RSSItem mItem;

    public RSSDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mItem = getArguments().getParcelable("item");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rss_detail, container, false);

        TextView titleView = view.findViewById(R.id.rss_detail_title);
        TextView descriptionView = view.findViewById(R.id.rss_detail_description);
        ImageView imageView = view.findViewById(R.id.rss_detail_image);

        titleView.setText(mItem.getTitle());
        descriptionView.setText(mItem.getDescription());
        Picasso.get().load(mItem.getImageUrl()).fit().centerCrop().into(imageView);

        return view;
    }
}
