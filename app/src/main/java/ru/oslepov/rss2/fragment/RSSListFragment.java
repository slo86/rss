package ru.oslepov.rss2.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import ru.oslepov.rss2.R;
import ru.oslepov.rss2.RSSItem;
import ru.oslepov.rss2.adapter.RSSListAdapter;
import ru.oslepov.rss2.adapter.RSSListAdapter.OnItemClickListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class RSSListFragment extends Fragment {
    private ArrayList<RSSItem> mItems;
    private OnItemClickListener mListener;

    public RSSListFragment() {
        // Required empty public constructor
    }

    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mListener = (OnItemClickListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mItems = getArguments().getParcelableArrayList("items");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rss_list, container, false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        RSSListAdapter adapter = new RSSListAdapter(getContext(), mItems);
        adapter.setOnItemClickListener(mListener);

        RecyclerView recyclerView = view.findViewById(R.id.rss_recycler_list);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);

        return view;
    }
}
