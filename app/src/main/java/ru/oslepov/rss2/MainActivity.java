package ru.oslepov.rss2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import com.facebook.stetho.Stetho;
import java.util.ArrayList;
import ru.oslepov.rss2.adapter.RSSListAdapter;
import ru.oslepov.rss2.fragment.RSSDetailFragment;
import ru.oslepov.rss2.fragment.RSSListFragment;
import ru.oslepov.rss2.receiver.RSSResultReceiver;
import ru.oslepov.rss2.service.RSSIntentService;
import ru.oslepov.rss2.service.RSSJobService;

public class MainActivity extends AppCompatActivity implements RSSResultReceiver.Receiver,
        RSSListAdapter.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {
    RSSResultReceiver mReceiver;

    public static final String IS_JOB_FIRST_RUN = "job scheduled";
    SwipeRefreshLayout mRefreshView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // debug
        Stetho.initializeWithDefaults(this);

        mReceiver = new RSSResultReceiver(new Handler());
        mReceiver.setReceiver(this);

        mRefreshView = findViewById(R.id.swipe_refresh);
        mRefreshView.setOnRefreshListener(this);

        // start service
        RSSIntentService.startActionGetRss(this, mReceiver);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (preferences.getBoolean(IS_JOB_FIRST_RUN, true)) {
            ComponentName componentName = new ComponentName(this, RSSJobService.class);

            JobInfo info = new JobInfo.Builder(1, componentName)
                    .setPeriodic(15 * 60 * 1000L, 5 * 60 * 1000)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setPersisted(true)
                    .build();

            JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);

            if (scheduler.schedule(info) == JobScheduler.RESULT_SUCCESS) {
                Log.d("MainActivity", "Job scheduled");
            } else {
                Log.d("MainActivity", "Job scheduling failed");
            }

            preferences.edit().putBoolean(IS_JOB_FIRST_RUN, false).apply();
        }
    }

    @Override
    public void onListItemClick(Bundle bundle) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        RSSDetailFragment detailFragment = new RSSDetailFragment();
        detailFragment.setArguments(bundle);

        if (findViewById(R.id.rss_container) != null) {
            fragmentTransaction.replace(R.id.rss_container, detailFragment);
        } else {
            fragmentTransaction.replace(R.id.rss_detail_container, detailFragment);
        }

        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle bundle) {
        ArrayList<RSSItem> items = bundle.getParcelableArrayList("items");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (findViewById(R.id.rss_container) != null) {
            RSSListFragment listFragment = new RSSListFragment();
            listFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.rss_container, listFragment);
        } else {
            RSSListFragment listFragment = new RSSListFragment();
            listFragment.setArguments(bundle);

            RSSItem item = items.get(0);
            bundle.putParcelable("item", item);

            RSSDetailFragment detailFragment = new RSSDetailFragment();
            detailFragment.setArguments(bundle);

            fragmentTransaction.replace(R.id.rss_list_container, listFragment);
            fragmentTransaction.replace(R.id.rss_detail_container, detailFragment);
        }

        fragmentTransaction.commit();

        FrameLayout progressView = findViewById(R.id.progress_bar);
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void onReceiveProcess() {
        FrameLayout progressView = findViewById(R.id.progress_bar);
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onReceiveError() {
        FrameLayout progressView = findViewById(R.id.progress_bar);
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void onRefresh() {
        mRefreshView.setRefreshing(true);

        mRefreshView.postDelayed(new Runnable() {
            @Override
            public void run() {
                RSSIntentService.startActionGetRss(getApplicationContext(), mReceiver);
                mRefreshView.setRefreshing(false);
            }
        }, 1000);
    }
}
