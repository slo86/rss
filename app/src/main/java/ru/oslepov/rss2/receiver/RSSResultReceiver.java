package ru.oslepov.rss2.receiver;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;

public class RSSResultReceiver extends ResultReceiver {
    public static final int CODE_PROCESS = 1;
    public static final int CODE_RESULT = 2;
    public static final int CODE_ERROR = 3;

    private Receiver mReceiver;

    public interface Receiver {
        void onReceiveResult(int resultCode, Bundle resultData);
        void onReceiveProcess();
        void onReceiveError();
    }

    public RSSResultReceiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {

            Log.d("RSSResultReceiver", String.valueOf(resultCode));
            switch (resultCode) {
                case CODE_PROCESS:
                    mReceiver.onReceiveProcess();
                    break;
                case CODE_RESULT:
                    mReceiver.onReceiveResult(resultCode, resultData);
                    break;
                case CODE_ERROR:
                    mReceiver.onReceiveError();
                    break;
            }
        }
    }
}
