package ru.oslepov.rss2.service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;

public class RSSJobService extends JobService {
    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d("RSSJobService", "onStartJob");
        RSSIntentService.startActionUpdateRss(getApplicationContext());
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
