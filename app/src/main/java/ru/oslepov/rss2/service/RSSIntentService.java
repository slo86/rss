package ru.oslepov.rss2.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import ru.oslepov.rss2.RSSItem;
import ru.oslepov.rss2.database.RSSDatabaseHelper;
import ru.oslepov.rss2.receiver.RSSResultReceiver;

public class RSSIntentService extends IntentService {
    private static final String ACTION_UPDATE_RSS = "updateRss";
    private static final String ACTION_GET_RSS = "getRss";
    private static final String EXTRA_PARAM_RECEIVER = "receiver";

    public RSSIntentService() {
        super("RSSIntentService");
    }

    public static void startActionUpdateRss(Context context) {
        Intent intent = new Intent(context, RSSIntentService.class);
        intent.setAction(ACTION_UPDATE_RSS);
        context.startService(intent);
    }

    public static void startActionGetRss(Context context, RSSResultReceiver receiver) {
        Intent intent = new Intent(context, RSSIntentService.class);
        intent.setAction(ACTION_GET_RSS);
        intent.putExtra(EXTRA_PARAM_RECEIVER, receiver);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_UPDATE_RSS.equals(action)) {
                handleActionUpdateRss();
            } else if (ACTION_GET_RSS.equals(action)) {
                final ResultReceiver receiver = intent.getParcelableExtra(EXTRA_PARAM_RECEIVER);
                handleActionGetRss(receiver);
            }
        }
    }

    private void handleActionUpdateRss() {
        Log.d("RSS2_Service", "handleActionUpdateRss");

        try {
            updateRssFeed();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleActionGetRss(ResultReceiver receiver) {
        Log.d("RSS2_Service", "handleActionGetRss");

        RSSDatabaseHelper helper = new RSSDatabaseHelper(getApplicationContext());
        SQLiteDatabase db = helper.getWritableDatabase();

        if (helper.getCount(db) == 0) {
            try {
                receiver.send(RSSResultReceiver.CODE_PROCESS, null);
                updateRssFeed();
            } catch (Exception e) {
                receiver.send(RSSResultReceiver.CODE_ERROR, null);
                e.printStackTrace();
            }
        }

        db.close();

        ArrayList<RSSItem> items = getLocalRssFeed();

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("items", items);
        receiver.send(RSSResultReceiver.CODE_RESULT, bundle);
    }

    private void updateRssFeed() throws Exception {
        RSSDatabaseHelper helper = new RSSDatabaseHelper(getApplicationContext());
        SQLiteDatabase db = helper.getWritableDatabase();

        db.beginTransaction();
        helper.truncateTable(db);

        try {
            URL url = new URL("https://lenta.ru/rss");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            InputStream stream = conn.getInputStream();
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(stream);
            Element element = document.getDocumentElement();
            NodeList nodeList = element.getElementsByTagName("item");

            if (nodeList.getLength() > 0) {
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Element nodeEl = (Element) nodeList.item(i);
                    Element titleEl = (Element) nodeEl.getElementsByTagName("title").item(0);
                    Element descriptionEl = (Element) nodeEl.getElementsByTagName("description").item(0);
                    Element pubDateEl = (Element) nodeEl.getElementsByTagName("pubDate").item(0);
                    Element imageEl = (Element) nodeEl.getElementsByTagName("enclosure").item(0);

                    String title = titleEl.getFirstChild().getNodeValue();
                    String description = descriptionEl.getTextContent();
                    String createdAt = pubDateEl.getFirstChild().getNodeValue();
                    String imageUrl = imageEl.getAttributes().getNamedItem("url").getNodeValue();

                    helper.insertItem(db, title.trim(), description.trim(), imageUrl, createdAt.trim());
                }
            }

            db.setTransactionSuccessful();
        } catch (SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace();
            throw new Exception("Update RSS failure");
        } finally {
            db.endTransaction();
            db.close();
        }

        db.close();
    }

    private ArrayList<RSSItem> getLocalRssFeed() {
        RSSDatabaseHelper helper = new RSSDatabaseHelper(getApplicationContext());
        SQLiteDatabase db = helper.getWritableDatabase();

        ArrayList<RSSItem> items = new ArrayList<>();
        Cursor cursor = helper.getItems(db);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            items.add(new RSSItem(
                cursor.getLong(cursor.getColumnIndex(RSSItem.RSSItemEntry._ID)),
                cursor.getString(cursor.getColumnIndex(RSSItem.RSSItemEntry.COLUMN_TITLE)),
                cursor.getString(cursor.getColumnIndex(RSSItem.RSSItemEntry.COLUMN_DESCRIPTION)),
                cursor.getString(cursor.getColumnIndex(RSSItem.RSSItemEntry.COLUMN_IMAGE_URL)),
                cursor.getString(cursor.getColumnIndex(RSSItem.RSSItemEntry.COLUMN_CREATED_AT))
            ));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return items;
    }
}
