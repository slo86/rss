package ru.oslepov.rss2.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;
import ru.oslepov.rss2.RSSItem.RSSItemEntry;

public class RSSDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "rss";
    private static final int DATABASE_VERSION = 1;

    public RSSDatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + RSSItemEntry.TABLE_NAME);
        onCreate(db);
    }

    private void createTable(SQLiteDatabase db) {
        final String SQL = "CREATE TABLE " +
            RSSItemEntry.TABLE_NAME + " (" +
            RSSItemEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            RSSItemEntry.COLUMN_TITLE + " TEXT NOT NULL, " +
            RSSItemEntry.COLUMN_DESCRIPTION + " TEXT NOT NULL, " +
            RSSItemEntry.COLUMN_IMAGE_URL + " TEXT NOT NULL, " +
            RSSItemEntry.COLUMN_CREATED_AT + " EXT NOT NULL" +
        ");";

        db.execSQL(SQL);
    }

    public void insertItem(SQLiteDatabase db, String title, String description, String imageUrl, String createdAt) {
        ContentValues itemValues = new ContentValues();
        itemValues.put(RSSItemEntry.COLUMN_TITLE, title);
        itemValues.put(RSSItemEntry.COLUMN_DESCRIPTION, description);
        itemValues.put(RSSItemEntry.COLUMN_IMAGE_URL, imageUrl);
        itemValues.put(RSSItemEntry.COLUMN_CREATED_AT, createdAt);

        db.insert(RSSItemEntry.TABLE_NAME, null, itemValues);
    }

    public void truncateTable(SQLiteDatabase db) {
        db.execSQL("DELETE FROM " + RSSItemEntry.TABLE_NAME);
    }

    public int getCount (SQLiteDatabase db) {
        String countQuery = "SELECT  * FROM " + RSSItemEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public Cursor getItems (SQLiteDatabase db) {
        String query = "SELECT  * FROM " + RSSItemEntry.TABLE_NAME;
        return db.rawQuery(query, null);
    }
}
