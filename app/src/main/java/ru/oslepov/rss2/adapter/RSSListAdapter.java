package ru.oslepov.rss2.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import ru.oslepov.rss2.R;
import ru.oslepov.rss2.RSSItem;

public class RSSListAdapter extends RecyclerView.Adapter<RSSListAdapter.RSSListViewHolder> {
    private Context mContext;
    private ArrayList<RSSItem> mItems;
    private OnItemClickListener mListener;

    public RSSListAdapter(Context context, ArrayList<RSSItem> items) {
        mContext = context;
        mItems = items;
    }

    static public class RSSListViewHolder extends RecyclerView.ViewHolder {
        public TextView titleView;
        public TextView descriptionView;
        public ImageView imageView;

        public RSSListViewHolder(@NonNull View itemView, final ArrayList<RSSItem> items,
                                 final OnItemClickListener listener) {
            super(itemView);

            titleView = itemView.findViewById(R.id.rss_item_title);
            descriptionView = itemView.findViewById(R.id.rss_item_description);
            imageView = itemView.findViewById(R.id.rss_item_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();

                        if (position != RecyclerView.NO_POSITION) {
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("item", items.get(position));
                            listener.onListItemClick(bundle);
                        }
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onListItemClick (Bundle bundle);
    }

    public void setOnItemClickListener (OnItemClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public RSSListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = android.view.LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.rss_item, parent, false);
        return new RSSListViewHolder(view, mItems, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RSSListViewHolder holder, int position) {
        String title = mItems.get(position).getTitle();
        String description = mItems.get(position).getDescription();
        String imageUrl = mItems.get(position).getImageUrl();

        holder.titleView.setText(title);
        holder.descriptionView.setText(description);

        if (holder.imageView != null) {
            Picasso.get().load(imageUrl).fit().centerCrop().into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}
